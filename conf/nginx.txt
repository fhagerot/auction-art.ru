server {
listen 80;
listen [::]:80;
root /home/vyatich/projects/auction-art.ru/public;
index index.php index.html index.htm;
server_name auction-art.ru;

client_max_body_size 5m;

return 301 https://auction-art.ru$request_uri;
location / {
try_files $uri $uri/ /index.php?$query_string;
}

location ~ .php$ {
include snippets/fastcgi-php.conf;
fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
}
}

server {
listen 443;
listen [::]:443;
root /home/vyatich/projects/auction-art.ru/public;
index index.php index.html index.htm;
server_name auction-art.ru;

ssl on;
ssl_certificate /etc/ssl/auction-art.ru/res.crt;
ssl_certificate_key /etc/ssl/auction-art.ru/private.key;

client_max_body_size 5m;

location / {
try_files $uri $uri/ /index.php?$query_string;
}

location ~ .php$ {
include snippets/fastcgi-php.conf;
fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
}
}
