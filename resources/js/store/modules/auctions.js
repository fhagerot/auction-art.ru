import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        items: [],
        item: false,
        current_bid: 0,
        bids: false,
        pagination: {},
        pictures: [],
        zoomCarouselData: [],
        mainImageSrc: false,
        showBidded: false,
        wrongBidMessage: false,
        inputedBid: false,
        counter: false,
        placeBid: false,
        sort: 'newest',
        show_all: true
    },
    getters: {
        showBidded(state){
            return state.showBidded;
        },
        pictures(state){
            return state.pictures;
        },
        zoomCarouselData(state){
            return state.zoomCarouselData;
        },
        items(state){
            return state.items;
        },
        itemsMap(state){
            let itemsMap = {};

            for(let i = 0; i < state.items.length; i++){
                let auction = state.items[i];
                itemsMap[auction.id] = auction;
            }

            return itemsMap;
        },
        item(state){
            return state.item;
        },
        mainImageSrc(state){
            return state.mainImageSrc;
        },
        bids(state){
            return state.bids;
        },
        placeBid(state){
            return state.placeBid;
        },
        pagination(state){
            return state.pagination;
        },
        current_bid(state){
            return state.current_bid;
        },
        inputedBid(state){
            return state.inputedBid;
        },
        wrongBidMessage(state){
            return state.wrongBidMessage;
        },
        counter(state){
            return state.counter;
        },
        sort(state){
            return state.sort;
        },
        show_all(state){
            return state.show_all;
        },
    },
    mutations: {
        clearItems(state){
            state.items = [];
        },
        loadNewItems(state, data){
            state.items = data;
        },
        loadItems(state, data){
            if (!data.data) {
                return;
            }
            // Вычисляем место ставки
            if (this._vm.$auth.user()) {
                let role = this._vm.$auth.user().role;
                let userId = this._vm.$auth.user().id;
                for (let i = 0; i < data.data.length; i++) {
                    if (role == this._vm.$customer && data.data[i].bids.length != 0) {
                        let bidPlace = -1;
                        for (let j = 0; j < data.data[i].bids.length; j++) {
                            if (data.data[i].bids[j].user_id == userId) {
                                bidPlace = j;
                            }
                        }
                        data.data[i].bid_place = bidPlace == -1 ? 0 : data.data[i].bids.length - bidPlace;
                    }
                    switch (role) {
                        case this._vm.$artist:
                            if (data.data[i].active == 1) {
                                data.data[i].link = Vue.router.resolve({name: 'auction', params: {id: data.data[i].id}}).href;
                            } else {
                                data.data[i].link =  Vue.router.resolve({name: 'auction.edit', params: {id: data.data[i].id}}).href;
                            }
                            break;
                        case this._vm.$customer:
                            data.data[i].link =  Vue.router.resolve({name: 'auction', params: {id: data.data[i].id}}).href;
                            break;
                        case this._vm.$admin:
                            data.data[i].link =  Vue.router.resolve({name: 'admin.auction', params: {id: data.data[i].id}}).href;
                            break;
                        default:
                            data.data[i].link =  Vue.router.resolve({name: 'auction', params: {id: data.data[i].id}}).href;
                            break;

                    }
                }
            }
            state.items = data.data;
            state.pagination = {
                current_page: data.current_page,
                from: data.from,
                to: data.to,
                total: data.total,
                prev_page: data.prev_page_url,
                next_page: data.next_page_url,
                last_page: data.last_page,
                per_page: data.per_page
            }
        },
        loadAdminItems(state, data){
            state.items = data.data;
            state.pagination = {
                current_page: data.current_page,
                from: data.from,
                to: data.to,
                total: data.total,
                prev_page: data.prev_page_url,
                next_page: data.next_page_url,
                last_page: data.last_page,
                per_page: data.per_page
            }
        },
        clearItem(state){
            state.item = false;
        },
        loadItem(state, data){
            state.item = data;
            state.counter = state.item.counter + 1;
            state.current_bid = state.item.bids[state.item.bids.length-1] === undefined ? state.item.price : state.item.bids[state.item.bids.length-1].sum;
            state.bids = state.item.bids;
            state.pictures = [];
            for (let i = 0; i < state.item.images.length; i++) {
                Vue.set(state.pictures, i, {
                    'key': i,
                    'src': state.item.images[i].path,
                    'src_min': state.item.images[i].path_min
                });
            }
            state.zoomCarouselData = [];
            for (let i = 0; i < state.item.images.length; i++) {
                Vue.set(state.zoomCarouselData, i, "<div class='bigImageItem'><img src ='"+state.item.images[i].path+"'/></div>");
            }
            state.mainImageSrc = state.item.images[0].path;
        },
        setCurrentBid(state, data) {
            state.current_bid = data.bid.sum;
            Vue.set(state.bids, state.bids.length, data.bid);
        },
        setMainImageSrc(state, data) {
            state.mainImageSrc = data.src;
        },
        setShowBidded(state, data) {
            state.showBidded = data;
        },
        setWrongBidMessage(state, data) {
            state.wrongBidMessage = data;
        },
        setInputedBid(state, data){
            state.inputedBid = data;
        },
        setCounter(state, data){
            state.counter = data;
        },
        setSort(state, data){
            state.sort = data;
        },
        setShowAll(state, data){
            state.show_all = data;
        }
    },
    actions: {
        loadItems(store, data){
            store.commit('clearItems');
            let sort = data.sort;
            let show_all = data.show_all;
            return Promise.resolve(Vue.axios.get('/auctions?page=' + data.page + '&sort=' + data.sort + '&show_all=' + show_all)
                .then(data => {
                    store.commit('loadItems', data.data);
                    store.commit('setSort', sort);
                }));
        },
        loadAdminItems(store, page){
            store.commit('clearItems');
            Vue.axios.get('/admin/auctions?page=' + page)
                .then(data => {
                    store.commit('loadAdminItems', data.data);
                });
        },
        setInputedBid(state, data){
            store.commit('setInputedBid', data);
        },
        setCounter(state, data){
            store.commit('setCounter', data);
        },
        setMainImageSrc(store, data) {
            store.commit('setMainImageSrc', data);
        },
        setWrongBidMessage(store, data) {
            store.commit('setWrongBidMessage', data);
        },
        loadItem(store, id) {
            store.commit('clearItem');
            return Promise.resolve(Vue.axios.get('/auctions/' + id)
                .then(data => {
                    store.commit('loadItem', data.data);
                })
            );
        },
        loadNewAuctions(store, data) {
            store.commit('clearItems');
            Vue.axios.get('/newauctions')
                .then(data => {
                    store.commit('loadNewItems', data.data);
                });
        },
        loadMyItems(store, page) {
            store.commit('clearItems');
            return Promise.resolve(Vue.axios.get('/dashboard/getmyauctions?page=' + page)
                .then(data => {
                    store.commit('loadItems', data.data);
                }));
        },
        bid(store, data){
            let bid = data.bid;
            let app = this;
            return Promise.resolve(Vue.axios({
                method: 'post',
                url: '/auctions/' + data.id+'/bid',
                data: {
                    bid: data.bid
                }
            })
            .then(data => {
                data = data.data;
                if (data.error) {
                    if (data.error == 'Unauthorized') {
                        app.dispatch('main/showAuthWithBackroute', Vue.router.currentRoute.fullPath);
                    }
                    if (data.error == 'default' && data.errorMessage) {
                        store.commit('setWrongBidMessage', data.errorMessage);
                    }
                }
                if(data.success == 1) {
                    store.commit('setCurrentBid', data);
                    store.commit('setShowBidded', true);
                    store.commit('loadItem', data.auction);
                }
                store.commit('setInputedBid', false);
            }));
        },
        updateAuction(store, data) {
            let formData = new FormData();
            data.files.forEach((f, x) => {
                if (f.id) {
                    f.position = x;
                    formData.append('filesToChange[]', JSON.stringify(f));
                } else {
                    f.position = x;
                    formData.append('files[]', f);
                }
            });
            formData.append('id', data.id);
            formData.append('end_date', data.end_date);
            formData.append('step', data.step);
            formData.append('price', data.price);
            formData.append('name', data.name);
            formData.append('description', data.description);
            if ('active' in data) {
                let active = data.active ? 1 : 0;
                formData.append('active', active);
            }
            if ('payed' in data) {
                let payed = data.payed ? 1 : 0;
                formData.append('payed', payed);
            }
            return Promise.resolve(Vue.axios({
                method: 'post',
                url: '/dashboard/updateAuction',
                data: formData
            })
            .then(data => {
                data = data.data;
                if (data['success']) {
                    location.reload();
                }
            }));
        },
        saveAuction(store, data){
            let formData = new FormData();
            let app = this;
            data.files.forEach((f,x) => {
                formData.append('files[]', f);
            });
            formData.append('end_date', data.end_date);
            formData.append('step', data.step);
            formData.append('price', data.price);
            formData.append('name', data.name);
            formData.append('description', data.description);
            return Promise.resolve( Vue.axios({
                method: 'post',
                url: '/dashboard/saveAuction',
                data: formData
            })
            .then(data => {
                data = data.data;
                if (data['success']) {
                    Vue.router.push(Vue.router.resolve({name: 'auctions.client'}).href);
                }
            }));
        }
    }
};
