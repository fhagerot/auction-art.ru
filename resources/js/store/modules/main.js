import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        showAuth: false,
        authBackroute: '',
        exceptLogout: false,
        has_error: false,
        errors: {},
        zoomOneImage: false,
        bigImageSrc: '',
        breadcrumbCustomTitle: false,
    },
    getters: {
        showAuth(state){
            return state.showAuth;
        },
        authBackroute(state){
            return state.authBackroute;
        },
        exceptLogout(state){
            return state.exceptLogout;
        },
        has_error(state){
            return state.has_error;
        },
        errors(state){
            return state.errors;
        },
        bigImageSrc(state){
            return state.bigImageSrc;
        },
        zoomOneImage(state){
            return state.zoomOneImage;
        },
        breadcrumbCustomTitle(state){
            return state.breadcrumbCustomTitle;
        }
    },
    mutations: {
        setShowAuth(state, data) {
            state.showAuth = data;
        },
        setAuthBackroute(state, data) {
            state.authBackroute = data;
        },
        setExceptLogout(state, data){
            state.exceptLogout = data;
        },
        setHasError(state, data){
            state.has_error = data;
        },
        setErrors(state, data){
            state.errors = data;
            setTimeout(function(){
                let el = document.getElementsByClassName('help-block')[0];
                if (el) {
                    (el).closest('.single-input-item').scrollIntoView(true);
                }
            }, 10)
        },
        setZoomOneImage(state, data) {
            state.zoomOneImage = data;
        },
        setBigImageSrc(state, data) {
            state.bigImageSrc = data;
        },
        setBreadcrumbCustomTitle(state, data) {
            state.breadcrumbCustomTitle = data;
        }
    },
    actions: {
        closeAuth(store, data) {
            store.commit('setShowAuth', false);
        },
        showAuth(store, data) {
            store.commit('setShowAuth', true);
        },
        showAuthWithBackroute(store, data) {
            store.commit('setShowAuth', true);
            store.commit('setAuthBackroute', data);
        },
    }
};
