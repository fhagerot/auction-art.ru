import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        dialogName: '',
        messages: [],
        dialogs: []
    },
    getters: {
        messages(state){
            return state.messages;
        },
        dialogs(state){
            return state.dialogs;
        },
        dialogName(state){
            return state.dialogName;
        },
    },
    mutations: {
        setMessages(state, data){
            state.messages = data;
        },
        setDialogs(state, data){
            state.dialogs = data;
        },
        setDialogName(state, data){
            state.dialogName = data;
        }
    },
    actions: {
        pushMessage(store, data) {
            return Promise.resolve(store.state.messages.push(data));
        },
        message(store, data) {
            let formData = new FormData();
            data.files.forEach((f,x) => {
                formData.append('files[]', f);
            });
            formData.append('message', data.message);
            formData.append('user_to_id', data.user_to_id);
            return Promise.resolve(
                Vue.axios({
                    method: 'post',
                    url: '/dashboard/message',
                    data: formData
                })
                .then(data => {
                    if (data.data.success) {
                        store.state.messages.push(data.data.message);
                    }
                })
            );
        },
        loadMessages(store, user_to_id) {
            return Promise.resolve(
                Vue.axios.post('/dashboard/loadMessages', {
                    user_to_id: user_to_id
                }).then(data => {
                    data = data.data;
                    store.commit('setMessages', data.messages);
                    store.commit('setDialogName', data.dialog.name);
                })
            );
        },
        loadDialogs(store, data) {
            Vue.axios.post('/dashboard/loadDialogs')
                .then(data => {
                store.commit('setDialogs', data.data.dialogs);
            });
        }
    }
};
