import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        roles: [],
        users: [],
        pagination: {},
    },
    getters: {
        roles(state){
            return state.roles;
        },
        users(state){
            return state.users;
        },
        pagination(state){
            return state.pagination;
        },
    },
    mutations: {
        setRoles(state, data){
            let roles =[];
            Object.keys(data).forEach(function (key){
                roles.push({label: data[key], code: key});
            });
            state.roles = roles;
        },
        setUsers(state, data){
                state.users = data.data;
                state.pagination = {
                    current_page: data.current_page,
                    from: data.from,
                    to: data.to,
                    total: data.total,
                    prev_page: data.prev_page_url,
                    next_page: data.next_page_url,
                    last_page: data.last_page,
                    per_page: data.per_page
                }

        },
    },
    actions: {
        loadClientRoles(store){
            return Promise.resolve(Vue.axios.get('/user/clientroles')
                .then(data => {
                    data = data.data;
                    store.commit('setRoles', data.clientRoles);
                })
            );
        },
        getAllUsers(store, page){
            return Promise.resolve(Vue.axios.post('/dashboard/user/all?page=' + page)
                .then(data => {
                    data = data.data;
                    store.commit('setUsers', data.users);
                })
            );
        }
    }
};
