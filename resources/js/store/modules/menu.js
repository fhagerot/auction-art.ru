import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        items: []
    },
    getters: {
        items(state){
            return state.items;
        },
    },
    mutations: {
        setMenu(state, data){
            if (this._vm.$auth.user()) {
                let role = this._vm.$auth.user().role;
            }
            state.items = data;
        }
    },
    actions: {
        generateMenu(store, data){
            let routes = [
                {
                    routename: 'auctions.list',
                    text: 'Аукционы'
                }
            ];
            /*if (this._vm.$auth.user()) {
                let role = this._vm.$auth.user().role;
                console.log(role);
                let userId = this._vm.$auth.user().id;
                console.log(userId);
                switch(role){
                    case this._vm.$artist:
                        routes.push(
                            {
                                routename: 'auction.create',
                                text: 'Продать'
                            });
                        break;
                    case this._vm.$customer:
                        routes.push(
                            {
                                routename: 'auctions.list',
                                text: 'Купить'
                            });
                        break;
                    case this._vm.$admin:
                        break;
                    default:
                        routes.push(
                            {
                                routename: 'register',
                                text: 'Продать'
                            },
                            {
                                routename: 'auctions.list',
                                text: 'Купить'
                            });
                }
            }*/
            store.commit('setMenu', routes);
        }
    },

};
