import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

Vue.prototype.$customer = 1;
Vue.prototype.$admin = 2;
Vue.prototype.$artist = 3;

import user from './modules/user';
import menu from './modules/menu';
import auctions from './modules/auctions';
import chat from './modules/chat';
import main from './modules/main';

const store = new Vuex.Store({
    modules: {
        main,
        user,
        menu,
        auctions,
        chat
    }
});
export default store;
