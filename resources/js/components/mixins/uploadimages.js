export const uploadimages = {
    data() {
        return {
            files: [],
            dragging: false,
        }
    },
    methods: {
        uploadFieldChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            ([...files]).forEach(f => {
                let reader = new FileReader();
                reader.readAsDataURL(f);
                reader.onloadend = () =>{
                    f.img = reader.result;
                    if (f.size/1024/1024 > 1) {
                        this.$store.commit('main/setHasError', true);
                        this.$store.commit('main/setErrors', {
                            files: {
                                0: 'Максимальный размер загружаемого изображения: 1Мб'
                            }
                        });
                        return;
                    }
                    this.files.push(f);
                }
            });
        },
        addFile(e) {
            let droppedFiles = e.dataTransfer.files;
            if(!droppedFiles) return;
            let imageType = /image.*/;
            ([...droppedFiles]).forEach(f => {
                if (f.type.match(imageType)) {
                    let reader = new FileReader();
                    reader.readAsDataURL(f);
                    reader.onloadend = () => {
                        f.img = reader.result;
                        if (f.size/1024/1024 > 1) {
                            this.$store.commit('main/setHasError', true);
                            this.$store.commit('main/setErrors', {
                                files: {
                                    0: 'Максимальный размер загружаемого изображения: 1Мб'
                                }
                            });
                            return;
                        }
                        this.files.push(f);
                    }
                }
            });
            this.dragging=false;
        },
        removeFile(file){
            if (file.deleted !== undefined && file.deleted === false) {
                file.deleted = true;
            } else {
                this.files = this.files.filter(f => {
                    return f != file;
                });
            }
        },
    }
}
