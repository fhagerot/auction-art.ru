import 'es6-promise/auto';
import axios from 'axios';
import './bootstrap';
import Vue from 'vue';
import VueAuth from '@websanova/vue-auth';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import auth from './auth';
import store from './store/index.js';
import router from './router';
import {mapGetters} from 'vuex';
import Meta from 'vue-meta';
import VueMasonry from 'vue-masonry-css'

Vue.use(VueMasonry);

// Set Vue globally;
window.Vue = Vue;

// Set Vue router
Vue.router = router;
Vue.use(VueRouter);
Vue.use(VueMasonry);

// Set Vue authentication
Vue.use(VueAxios, axios);

if (process.env.MIX_ENV == 'production') {
    axios.defaults.baseURL = `https://auction-art.ru/api`;
} else {
    axios.defaults.baseURL = `http://auction-art.ru/api`;
}
Vue.use(VueAuth, auth);
Vue.use(Meta);

function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);
    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}

const scrollWidthConst = scrollbarWidth();
let hidden = false;
Vue.mixin({
    methods: {
        numbers(event) {
            return event.charCode >= 48 && event.charCode <= 57;
        },
        showZoomImage(src){
            this.$store.commit('main/setZoomOneImage', true);
            this.$store.commit('main/setBigImageSrc', src);
            this.hideMainScroll();
        },
        hideMainScroll(){
            if(!hidden){
                document.querySelector('body').style.marginRight = scrollWidthConst + 'px';
                document.querySelector('body').style.overflow = 'hidden';
                document.querySelector('html').style.overflow = 'hidden';
                hidden = true;
            }
        },
        showMainScroll(){
            document.querySelector('body').style.marginRight = '0px';
            document.querySelector('body').style.overflow = 'auto';
            document.querySelector('html').style.overflow = 'auto';
            hidden = false;
        },
        showAuthWithBackroute(backroute) {
            this.$store.dispatch('main/showAuthWithBackroute', backroute);
        }
    },
    destroyed() {
        this.$store.commit('main/setHasError', false);
        this.$store.commit('main/setErrors', {});
    },
    computed: {
        ...mapGetters('main', {
            errors: 'errors',
            has_error: 'has_error',
        })
    }
})

// Load Index
Vue.component('index', require('./components/Index.vue').default);
import VueCarousel from '@chenfengyuan/vue-carousel';
Vue.component(VueCarousel.name, VueCarousel);
axios.interceptors.response.use(
    function(response) {
        if (response.data['error']) {
            store.commit('main/setHasError', true);
            if (response.data['error'] == 'not_found') {
                Vue.router.push(Vue.router.resolve({name: 'error404'}).href);
                return;
            }
            if (response.data['errors']) {
                store.commit('main/setErrors', response.data['errors']);
            }
        }
        if (((Vue.auth.user().id || Vue.auth.check()) && response.data['error'] == 'Unauthorized') || (Vue.auth.check() && !Vue.auth.user().id)) {
            Vue.auth.logout();
            return;
        }
        if (response.data.redirect) {
            window.location = response.data.redirect;
        }
        return response;
    }, function(error) {
});
const app = new Vue({
    el: '#app',
    store,
    router
});

//require('./parlo/js/vendor/modernizr-2.8.3.min.js');
//require( './parlo/js/popper.min.js');
//require('./parlo/js/plugins.js');
require('./parlo/js/main.js');
