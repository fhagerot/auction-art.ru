import VueRouter from 'vue-router';
// Pages
import Home from './components/pages/Home';
import Register from './components/pages/Register';
import Login from './components/pages/Login';
import ResetPassword from './components/pages/ResetPassword';
import ChangePassword from './components/pages/ChangePassword';
import HowToBuy from './components/pages/HowToBuy';
import HowToSell from './components/pages/HowToSell';
import AboutCompany from './components/pages/AboutCompany';
import Dashboard from './components/pages/user/Dashboard';
import AdminDashboard from './components/pages/admin/Dashboard';
import Users from './components/pages/admin/Users';
import HomeAdmin from './components/pages/admin/HomeAdmin';
import AuctionsAdmin from './components/pages/admin/AuctionsAdmin';
import AdminUser from './components/pages/admin/AdminUser';
import Error404 from './components/pages/Error404';
import AuctionList from './components/pages/AuctionList';
import Auction from './components/pages/Auction';
import AuctionsClient from './components/pages/user/AuctionsClient';
import HomeClient from './components/pages/user/HomeClient';
import MessageBetweenClients from './components/pages/user/MessageBetweenClients';
import DialogsClient from './components/pages/user/DialogsClient';
import CreateAuctionClient from  './components/pages/user/artist/CreateAuctionClient';
import EditAuctionClient from  './components/pages/user/artist/EditAuctionClient';
// Routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined,
            title: 'Главная',
            meta: 'Ку'
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false,
            title: 'Регистрация'
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false,
            title: 'Вход'
        }
    },
    {
        path: '/reset-password',
        name: 'reset.password',
        component: ResetPassword,
        meta: {
            auth: false,
            title: 'Сброс пароля'
        }
    },
    {
        path: '/change-password/:access_hash',
        name: 'change.password',
        component: ChangePassword,
        meta: {
            auth: false,
            title: 'Изменение пароля'
        }
    },
    {
        path: '/auctions',
        name: 'auctions.list',
        component: AuctionList,
        meta: {
            auth: undefined,
            title: 'Аукционы'
        },
        beforeEnter(to, from, next) {
            if (to.query.page == 1) {
                to.query.page = undefined;
                next({path: to.path});
            } else {
                next();
            }
        }
    },
    {
        path: '/auctions/:id',
        name: 'auction',
        component: Auction,
        meta: {
            auth: undefined,
            title: 'Просмотр аукциона',
            breadcrumbs: [
                'auctions.list',
                'auction'
            ]
        }
    },
    {
        path: '/help/how-to-buy',
        name: 'how.to.buy',
        component: HowToBuy,
        meta: {
            auth: undefined,
            title: 'Как купить картину',

        }
    },
    {
        path: '/help/how-to-sell',
        name: 'how.to.sell',
        component: HowToSell,
        meta: {
            auth: undefined,
            title: 'Как продать картину',
        }
    },
    {
        path: '/about-company',
        name: 'about.company',
        component: AboutCompany,
        meta: {
            auth: undefined,
            title: 'О компании',
        }
    },
    // USER ROUTES
    {
        path: '/dashboard',
        component: Dashboard,
        meta: {
            auth:  {roles: [3,1], redirect: {name: 'home'}},
            title: 'Личный кабинет'
        },
        children: [
            {
                path: '',
                name: 'dashboard',
                component: HomeClient,
                meta: {
                    title: 'Домашняя'
                }
            },
            {
                path: 'auctions',
                name: 'auctions.client',
                component: AuctionsClient,
                meta: {
                    title: 'Мои аукционы'
                }
            },
            {
                path: 'auctions/create',
                name: 'auction.create',
                hideFromMenu: true,
                component: CreateAuctionClient,
                meta: {
                    auth:  {roles: 3, redirect: {name: 'dashboard'}},
                    title: 'Создание аукциона'
                }
            },
            {
                path: 'auctions/:id',
                name: 'auction.edit',
                hideFromMenu: true,
                component: EditAuctionClient,
                meta: {
                    auth:  {roles: 3, redirect: {name: 'dashboard'}},
                    title: 'Редактирование аукциона'
                }
            },
            {
                path: 'dialogs',
                name: 'dialogs.client',
                component: DialogsClient,
                meta: {
                    title: 'Диалоги'
                }
            },
            {
                path: 'dialogs/:to',
                name: 'message',
                component: MessageBetweenClients,
                hideFromMenu: true,
                meta: {
                    auth:  {roles: [3,1], redirect: {name: 'dashboard'}},
                    title: 'Диалоги'
                }
            }
        ]
    },
    // ADMIN ROUTES
    {
        path: '/admin',
        component: AdminDashboard,
        meta: {
            auth: {roles: 2, redirect: {name: 'home'}, forbiddenRedirect: '/403'},
            title: 'Домашняя'
        },
        children: [
            {
                path: '',
                name: 'admin.dashboard',
                component: HomeAdmin,
                meta: {
                    title: 'Домашняя'
                }
            },
            {
                path: 'auctions',
                name: 'auctions.admin',
                component: AuctionsAdmin,
                meta: {
                    title: 'Аукционы'
                }
            },
            {
                path: 'users',
                name: 'users.admin',
                component: Users,
                meta: {
                    title: 'Пользователи'
                }
            },
            {
                path: 'users/:id',
                name: 'user.admin.edit',
                component: AdminUser,
                hideFromMenu: true,
                meta: {
                    title: 'Редактирование пользователя',
                    breadcrumbs: [
                        'auctions.list',
                        'auction'
                    ],
                }
            },
            {
                path: 'auctions/:id',
                name: 'admin.auction',
                component: EditAuctionClient,
                hideFromMenu: true,
                meta: {
                    title: 'Редактирование аукциона'
                }
            },
            {
                path: 'dialogs',
                name: 'dialogs.admin',
                component: DialogsClient,
                meta: {
                    auth:  {roles: [2], redirect: {name: 'dashboard'}},
                    title: 'Диалоги'
                }
            },
            {
                path: 'dialogs/:to',
                name: 'message.admin',
                component: MessageBetweenClients,
                hideFromMenu: true,
                meta: {
                    auth:  {roles: [2], redirect: {name: 'dashboard'}},
                    title: 'Диалоги'
                }
            }
        ]
    },
    {
        path: '*',
        name: 'error',
        component: Error404,
        meta: {
            auth: undefined,
            title: 'Ошибка 404'
        }
    },
    {
        path: '/404',
        name: 'error404',
        component: Error404,
        meta: {
            auth: undefined,
            title: 'Ошибка 404'
        }
    },
];
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
    linkExactActiveClass: 'active',
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            document.getElementById('app').scrollIntoView();
            return null
        }
    }
});

export default router;
