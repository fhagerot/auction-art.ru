@extends('emails.layouts.app')

@section('content')
    <p style="text-align: center">
        {{$oUser->login}} Вы победили в аукционе — <a href="{{config('app.url') . '/auctions/' . $auction->id}}">{{$auction->name}}</a><br><br>
        Оплатите выигранную картину и напишите создателю аукциона<br/>
        <button class="btn">
            <a href="{{config('app.url') . '/dashboard/dialogs/1'}}">Оплатить</a>
        </button>
    </p>

@endsection
