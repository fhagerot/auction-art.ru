@extends('emails.layouts.app')

@section('content')
    <p style="text-align: center">
        {{$oUser->login}} чтобы сменить пароль перейдите по ссылке <br><br><a class="btn" href="{{ config('app.url') . '/change-password/' . $access_hash . '?email=' . $oUser->email}}">Сбросить пароль</a>
    </p>
@endsection
