@extends('emails.layouts.app')

@section('content')
    <p style="text-align: center">
        {{$oUser->login}} у аукциона <a href="{{config('app.url') . '/auctions/' . $auction->id}}">"{{$auction->name}}"</a> есть победитель — <a href="{{config('app.url') . '/dashboard/dialogs/' . $winner->id}}">{{$winner->login}}</a>
    </p>

@endsection
