@extends('emails.layouts.app')

@section('content')
    <p style="text-align: center">
        {{$oUser->login}} ваша ставка перебита в аукционе <a href="{{ config('app.url') . '/auctions/' . $auction->id}}">"{{$auction->name}}"</a><br/><br>
        Новая минимальная ставка: {{$lastBid->sum + $auction->step }} р.
    </p>
@endsection
