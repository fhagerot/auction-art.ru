<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://auction-art.ru/</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/auctions</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/login</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.25</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/register</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.25</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/help/how-to-sell</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.25</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/help/how-to-buy</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.25</priority>
    </url>
    <url>
        <loc>https://auction-art.ru/about-company</loc>
        <lastmod>2020-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.25</priority>
    </url>
    @foreach ($auctions as $auction)
        <url>
            <loc>https://auction-art.ru/auctions/{{ $auction->id }}</loc>
            <lastmod>{{ $auction->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
</urlset>
