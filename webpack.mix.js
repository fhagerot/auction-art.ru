const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var args = process.argv.slice(2);
if (!args.includes('--prerender')) {
    console.log('run frontend');
    mix.js('resources/js/app.js', 'public/js');
    mix.sass('resources/sass/app.scss', 'public/css')
        .options({
            processCssUrls: false
        });
} else {
    console.log('run prerender');
    var dynamicRoutes = [];
    var readlines = require('n-readlines');
    var liner = new readlines('./dynamic_routes.txt');
    var next;
    while (next = liner.next()) {
        dynamicRoutes.push(next.toString('utf-8').split(' ')[0].replace('\r', '').replace('\n', ''));
    }
    var staticRoutes = ['/auctions', '/register', '/', '/login', '/help/how-to-sell', '/help/how-to-buy', '/about-company'];
    if (args.includes('--prerender-static')) {
        console.log('run prerender-static');
        var allRoutes = staticRoutes;
    } else if (args.includes('--prerender-dynamic')) {
        console.log('run prerender-dynamic');
        const fs = require('fs');
        const path = './dynamic_routes.txt';

        fs.readFile(path, 'utf-8', (err, data) => {
            let lines = data.split('\n');
            lines.splice(0, 1);
            let splited = lines.join('\n');
            fs.writeFile(path, splited, err => {});
        })
        var allRoutes = dynamicRoutes.slice(0, 1);
    } else {
        console.log('run prerender-all');
        var allRoutes = [...dynamicRoutes, ...staticRoutes];
    }
    console.log(allRoutes);
    var path = require('path');
    var PrerenderSpaPlugin = require('prerender-spa-plugin');
    const Renderer = PrerenderSpaPlugin.PuppeteerRenderer;
    var msgHandler = function (route, handler) {
        console.log(route, handler.text())
    }
    mix.webpackConfig({
        plugins: [
            new PrerenderSpaPlugin({
                // Absolute path to compiled SPA
                staticDir: path.join(__dirname, 'public/'),
                outputDir: path.join(__dirname, 'public/prerendered'),
                routes: allRoutes,
                renderer: new Renderer({
                    renderAfterTime: 12000,
                    consoleHandler: msgHandler,
                    args: [
                        '--disable-web-security',
                    ],
                })
            })
        ],
    })
}
