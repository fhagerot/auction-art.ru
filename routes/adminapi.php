<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| ADMIN API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('admin')->middleware(['auth:api', 'admin'])->group(function () {
    Route::get('auctions', 'AuctionController@index');
    Route::post('getUser', 'UserController@getUser');
    Route::post('updateUser', 'UserController@updateUser');
});

