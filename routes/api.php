<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('changePassword', 'AuthController@changePassword');
    Route::post('resetPassword', 'AuthController@resetPassword');
    Route::get('refresh', 'AuthController@refresh');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});

Route::prefix('dashboard')->middleware('auth:api')->group(function () {
    Route::get('getmyauctions', 'AuctionController@getMyAuctions');
    Route::post('saveAuction', 'AuctionController@saveAuction');
    Route::post('updateAuction', 'AuctionController@updateAuction');
    Route::post('message', 'ChatController@message');
    Route::post('loadMessages', 'ChatController@loadMessages');
    Route::post('loadDialogs', 'ChatController@loadDialogs');
    Route::post('user/all', 'UserController@all');
});
Route::middleware('auth:api')->post('auctions/{id}/bid', 'AuctionController@bid');
Route::get('auctions', 'AuctionController@index');
Route::get('auctions/{id}', 'AuctionController@show');
Route::get('auctions/{id}/edit', 'AuctionController@edit');
Route::get('user/clientroles', 'AuthController@roles');
Route::get('newauctions', 'AuctionController@newAuctions');

