<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateAuctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description')->nullable();
            $table->unsignedInteger('user_id');
            $table->integer('price');
            $table->integer('step');
            $table->boolean('active')->default(0);
            $table->boolean('payed')->default(0);
            $table->dateTime('end_date')->nullable();
            $table->boolean('prerender_queued')->default(0);
            $table->boolean('message_sended')->default(0);
            $table->timestamps();
        });

        Schema::table('auctions', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
