<?php

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class AuctionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Auction::class, 50)->create()->each(function ($auction) {
            $auction->images()->save(factory(App\AuctionImage::class)->make());
            $auction->images()->save(factory(App\AuctionImage::class)->make());
            $auction->images()->save(factory(App\AuctionImage::class)->make());
            $auction->images()->save(factory(App\AuctionImage::class)->make());
            $auction->images()->save(factory(App\AuctionImage::class)->make());
        });
    }
}
