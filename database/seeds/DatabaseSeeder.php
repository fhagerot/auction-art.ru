<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Auction;
use App\AuctionImage;
use Carbon\Carbon;

// @codingStandardsIgnoreLine
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'login' => 'admin',
            'email' => 'auction@auction-art.ru',
            'password' => Hash::make('123456'),
            'role' => User::ADMIN
        ]);
        User::create([
            'login' => 'customer',
            'email' => 'customer@mail.ru',
            'password' => Hash::make('123456'),
            'role' => User::CUSTOMER
        ]);
        $artist = User::create([
            'login' => 'yurga1964',
            'email' => 'fhagerot23@yandex.ru',
            'password' => Hash::make('123456'),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Зима. Холст/масло 50x60 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 6000,
            'step' => 300,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina1.jpg',
            'path_min' => 'min_kartina1.jpg',
            'original_name' => 'kartina1.jpg',
            'auction_id' => $auction->id
        ]);
        $auction = Auction::create([
            'name' => 'Зимний свет. Холст/масло 35x45 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 2200,
            'step' => 200,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina2.jpg',
            'path_min' => 'min_kartina2.jpg',
            'original_name' => 'kartina2.jpg',
            'auction_id' => $auction->id
        ]);
        $auction = Auction::create([
            'name' => 'Весна в дунилово. Холст/масло 40x60 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 4000,
            'step' => 200,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina3.jpg',
            'path_min' => 'min_kartina3.jpg',
            'original_name' => 'kartina3.jpg',
            'auction_id' => $auction->id
        ]);
        $auction = Auction::create([
            'name' => 'Борисоглебскй монастырь. Холст/масло 75x75 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 8000,
            'step' => 300,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina4.jpg',
            'path_min' => 'min_kartina4.jpg',
            'original_name' => 'kartina4.jpg',
            'auction_id' => $auction->id
        ]);
        $auction = Auction::create([
            'name' => 'С полей. Холст/масло 30x50 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 2400,
            'step' => 200,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina6.jpg',
            'path_min' => 'min_kartina6.jpg',
            'original_name' => 'kartina6.jpg',
            'auction_id' => $auction->id
        ]);
        $auction = Auction::create([
            'name' => 'Весенний ручей. Холст/масло 45x45 см, Автор - Александр Юргин',
            'description' => '',
            'price' => 2500,
            'step' => 200,
            'end_date' => null,
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina5.jpg',
            'path_min' => 'min_kartina5.jpg',
            'original_name' => 'kartina5.jpg',
            'auction_id' => $auction->id
        ]);
        $this->call([
           AuctionsEndedSeeder::class
        ]);
    }
}
