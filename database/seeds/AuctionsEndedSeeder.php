<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\User;
use App\AuctionImage;
use App\Auction;
use App\Bid;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

// @codingStandardsIgnoreLine
class AuctionsEndedSeeder extends Seeder
{

    private function bidder($username)
    {
        $faker = Faker\Factory::create();
        return User::create([
            'login' => $username,
            'email' =>  $faker->unique()->email,
            'password' => Hash::make(Str::random(12)),
            'role' => User::CUSTOMER
        ]);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artist = User::create([
            'login' => 'juliaev',
            'email' => 'system1@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Запах целебных трав. 30х40 см. Оргалит, акрил. Автор - Александр Жулаев.',
            'description' => '',
            'price' => 900,
            'step' => 200,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(9)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina7.jpg',
            'path_min' => 'min_kartina7.jpg',
            'original_name' => 'kartina7.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('deadofwrite')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(14)->addMinutes(32)->subDays(10)->addSeconds(15)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('AlekseyNikiforov')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(9)->addMinutes(41)->subDays(9)->addSeconds(42)->format('Y-m-d H:i:s');
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step + 200;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'erg75',
            'email' => 'system2@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Мороз и солнце. 40х60 см. Масло/холст. Автор - Анатолий Ергунов.',
            'description' => '',
            'price' => 6500,
            'step' => 500,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(10)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina8.jpg',
            'path_min' => 'min_kartina8.jpg',
            'original_name' => 'kartina8.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('inkandfable')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(12)->addMinutes(41)->subDays(11)->addSeconds(34)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'mongodmit',
            'email' => 'system4@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Гуамское ущелье. 60х90 см. Масло/холст. Автор - Дмиртий Арсентьев.',
            'description' => '',
            'price' => 15000,
            'step' => 500,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(3)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina9.jpg',
            'path_min' => 'min_kartina9.jpg',
            'original_name' => 'kartina9.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('thedad')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(17)->addMinutes(30)->subDays(4)->addSeconds(14)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'Rose',
            'email' => 'system5@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Под крик чаек. 50х40 см. Масло/холст. Автор - Регина Хмелевская.',
            'description' => '',
            'price' => 1500,
            'step' => 200,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(1)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina10.jpg',
            'path_min' => 'min_kartina10.jpg',
            'original_name' => 'kartina10.jpg',
            'auction_id' => $auction->id
        ]);
        $usrId = self::bidder('paintlover')->id;
        $bid = new Bid();
        $bid->user_id = $usrId;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(7)->addMinutes(18)->subDays(2)->addSeconds(37)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('maximus')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(15)->addMinutes(11)->subDays(1)->addSeconds(44)->format('Y-m-d H:i:s');
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = $usrId;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(18)->addMinutes(53)->subDays(1)->addSeconds(45)->format('Y-m-d H:i:s');
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step + 500;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'DynArt',
            'email' => 'system6@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Степанаван. 50х70 см. Масло/холст. Автор - Дин Даллакян.',
            'description' => '',
            'price' => 1500,
            'step' => 200,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(15)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina11.jpg',
            'path_min' => 'min_kartina11.jpg',
            'original_name' => 'kartina11.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('sergey1974')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(11)->addMinutes(2)->subDays(16)->addSeconds(41)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'tayana_brag',
            'email' => 'system7@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Лазурное утро. 40х40 см. Масло/холст. Автор - Татьяна Брагина.',
            'description' => '',
            'price' => 1200,
            'step' => 200,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(4)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina12.jpg',
            'path_min' => 'min_kartina12.jpg',
            'original_name' => 'kartina12.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('filmforher')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(16)->addMinutes(26)->subDays(5)->addSeconds(43)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('serg')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(16)->addMinutes(20)->subDays(4)->addSeconds(19)->format('Y-m-d H:i:s');
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'IvanovAr',
            'email' => 'system8@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Зимний вечер. 40х70 см. Масло/холст. Автор - Иванов И.А.',
            'description' => '',
            'price' => 9500,
            'step' => 500,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(4)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina13.jpg',
            'path_min' => 'min_kartina13.jpg',
            'original_name' => 'kartina13.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('makSim')->id;
        $bid->sum = $auction->price;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(7)->addMinutes(55)->subDays(5)->addSeconds(33)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('domovoy_37')->id;
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(18)->addMinutes(17)->subDays(5)->addSeconds(28)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = $usrId;
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(13)->addMinutes(01)->subDays(5)->addSeconds(02)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'Stoyev',
            'email' => 'system9@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Утро в Лисьей бухте. 35х50 см. Масло/холст. Автор - Стоев Сергей',
            'description' => '',
            'price' => 5600,
            'step' => 500,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(6)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina14.jpg',
            'path_min' => 'min_kartina14.jpg',
            'original_name' => 'kartina14.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('rookiemag')->id;
        $bid->sum = $auction->price;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(15)->addMinutes(79)->subDays(7)->addSeconds(43)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'AnnaShkk',
            'email' => 'system10@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Утро. 30х40 см. Масло/холст. Автор - Анна Красикова',
            'description' => '',
            'price' => 1000,
            'step' => 200,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(12)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina15.jpg',
            'path_min' => 'min_kartina15.jpg',
            'original_name' => 'kartina15.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('_nataliya_')->id;
        $bid->sum = $auction->price;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(11)->addMinutes(17)->subDays(13)->addSeconds(6)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'puherik',
            'email' => 'system11@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Тройка. 60х40 см. Масло/холст. Автор - Эрика Пухова',
            'description' => '',
            'price' => 5100,
            'step' => 500,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(6)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina16.jpg',
            'path_min' => 'min_kartina16.jpg',
            'original_name' => 'kartina16.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('silver')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(13)->addMinutes(29)->subDays(7)->addSeconds(18)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'Solovey',
            'email' => 'system12@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Тайна Единорога. Ручки, карандаш/бумага. 41х51 см. Автор - Наталья Соловьева',
            'description' => '',
            'price' => 500,
            'step' => 300,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(9)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina17.jpg',
            'path_min' => 'min_kartina17.jpg',
            'original_name' => 'kartina17.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('Charodey')->id;
        $bid->sum = $auction->price;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(17)->addMinutes(51)->subDays(10)->addSeconds(34)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('evelina72')->id;
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(12)->addMinutes(24)->subDays(9)->addSeconds(14)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();


        $artist = User::create([
            'login' => 'bone',
            'email' => 'system13@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Волна. 25х35 см. Масло/грунтованный картон. Автор - Владимир Костючек',
            'description' => '',
            'price' => 1000,
            'step' => 300,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(16)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina18.jpg',
            'path_min' => 'min_kartina18.jpg',
            'original_name' => 'kartina18.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('SSSR')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(14)->addMinutes(24)->subDays(17)->addSeconds(39)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $artist = User::create([
            'login' => 'Outkina',
            'email' => 'system14@auction-art.ru',
            'password' => Hash::make(Str::random(12)),
            'role' => User::ARTIST
        ]);
        $auction = Auction::create([
            'name' => 'Мраморные львы. акварель с белилами на бумаге Bockingford 300g. 30х40 см. Автор - Наталья О.',
            'description' => '',
            'price' => 3000,
            'step' => 300,
            'end_date' =>  Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->subDays(12)->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->startOfDay()->addHours(21)->addMinutes(2)->subDays(36)->format('Y-m-d H:i:s'),
            'user_id' => $artist->id,
            'active' => 1
        ]);
        AuctionImage::create([
            'path' => 'kartina19.jpg',
            'path_min' => 'min_kartina19.jpg',
            'original_name' => 'kartina19.jpg',
            'auction_id' => $auction->id
        ]);
        $bid = new Bid();
        $bid->user_id = self::bidder('Annushka')->id;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(17)->addMinutes(8)->subDays(13)->addSeconds(19)->format('Y-m-d H:i:s');
        $bid->sum = $auction->price;
        $bid->auction_id = $auction->id;
        $bid->save();

        $bid = new Bid();
        $bid->user_id = self::bidder('july_rose')->id;
        $bid->sum = $auction->bids()->latest()->first()->sum + $auction->step;
        $bid->created_at = Carbon::now()->startOfDay()->addHours(19)->addMinutes(44)->subDays(13)->addSeconds(11)->format('Y-m-d H:i:s');
        $bid->auction_id = $auction->id;
        $bid->save();

    }
}
