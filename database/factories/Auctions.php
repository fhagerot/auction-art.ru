<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Auction::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'description' => $faker->text(),
        'price' => rand(100, 1000),
        'step' => round(rand(100, 300) / 100) * 100,
        'end_date' => Carbon::now()->addHours(rand(20, 200))->format('Y-m-d H:i:s'),
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'active' => 1
    ];
});
