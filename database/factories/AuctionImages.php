<?php

use Faker\Generator as Faker;

$factory->define(App\AuctionImage::class, function (Faker $faker) {
    $images = ['9.jpg', '8.jpg', '7.jpg', '2.jpg'];
    $i = array_rand($images);
    return [
        'path' => $images[$i],
        'path_min' => 'min_' . $images[$i],
        'original_name' => 'Картина'
    ];
});
