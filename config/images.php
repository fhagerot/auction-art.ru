<?php

return [
    'max_size' => 350,
    'normal_size' => 1000,
    'allowed_extensions' => [
        'jpg', 'jpeg', 'png', 'bmp'
    ]
];
