<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Modules\Media\Entities\File;

class USerController extends Controller
{
    public function all(Request $request)
    {
        $users = User::withoutGlobalScope('active')->paginate(15);
        $resData['users'] = $users;
        $resData['success'] = true;
        return response()->json($resData);
    }
}
