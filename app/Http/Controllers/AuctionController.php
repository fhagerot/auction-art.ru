<?php

namespace App\Http\Controllers;

use App\AuctionImage;
use App\Bid;
use App\Dialog;
use App\Message;
use App\Traits\ValidationTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Auction;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Media\Entities\File;
use Symfony\Component\Console\Input\Input;
use Intervention\Image\Facades\Image;

class AuctionController extends Controller
{
    use ValidationTrait;

    public function index()
    {
        $sort = $_GET['sort'] ?? 'newest';
        $show_all = $_GET['show_all'] ?? 'false';
        $query =  Auction::with('images', 'bids', 'user')
            ->where('active', 1);
        if ($show_all == 'false') {
            Log::info('2');
            $query->where(function ($query) {
                $query->where('end_date', '>', Carbon::now()->format('Y-m-d H:i:s'))
                    ->orWhereNull('end_date');
            });
        }
        switch ($sort) {
            case 'newest':
                $query = $query->orderBy('created_at', 'desc')->orderBy('id', 'desc');
                break;
            case 'expired':
                $query = $query->orderBy('end_date', 'asc');
                break;
            /*case 'price_up':
                $query = $query->withCount('bids')->orderBy('bids_count', 'asc');
                break;
            case 'price_down':
                $query = $query->withCount('bids')->orderBy('bids_count', 'desc');
                break;*/
            case 'bids_up':
                $query = $query->withCount('bids')->orderBy('bids_count', 'asc');
                break;
            case 'bids_down':
                $query = $query->withCount('bids')->orderBy('bids_count', 'desc');
                break;
        }
        $auctions = $query->paginate(15);
        return response()->json($auctions);
    }

    public function show($id)
    {
        $auction = Auction::with('images', 'user', 'bids.user:id,login')->find($id);
        if (!$auction) {
            return response()->json(['error' => 'not_found']);
        }
        return response()->json($auction);
    }

    public function edit($id)
    {
        $auction = Auction::with('images', 'bids', 'user')->find($id);
        if (!$auction) {
            return response()->json(['error' => 'not_found']);
        }
        if ($auction->active && Auth::user()->role != User::ADMIN) {
            return response()->json(['redirect' => '/auctions/' . $auction->id]);
        }
        return response()->json($auction);
    }

    public function getMyAuctions()
    {
        switch (Auth::user()->role) {
            case User::ARTIST:
                $auctions = Auction::with('images', 'bids', 'user')
                    ->where('user_id', Auth::user()->id)
                    ->orderBy('created_at', 'desc')
                    ->paginate(15);
                break;
            case User::CUSTOMER:
                $auctions = Auction::with('images', 'bids', 'user')
                    ->whereHas('bids', function ($q) {
                        $q->where('user_id', Auth::user()->id);
                    })->orderBy('created_at', 'desc')->paginate(15);
                break;
            default:
                $auctions = [];
                break;
        }
        return response()->json($auctions);
    }

    public function newAuctions()
    {
        $auctions = Auction::with('images', 'bids', 'user')
            ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->where(function ($query) {
                $query->where('end_date', '>', Carbon::now()->format('Y-m-d H:i:s'))
                    ->orWhereNull('end_date');
            })
            ->limit(3)
            ->get();
        return response()->json($auctions);
    }

    public function bid($id, Request $request)
    {
        $user = Auth::user();
        $userId = $user->id;
        $resData['success'] = false;
        if (Auth::user()->role !== User::CUSTOMER) {
            $resData['error'] =  'default';
            $resData['errorMessage'] = 'Вы должны быть обычным пользователем (покупатель), чтобы иметь возможность делать ставки.';
            return response()->json($resData);
        }
        $auction = Auction::find($id);
        if ($auction->end_date !== null && $auction->end_date < Carbon::now()->format('Y-m-d H:i:s')) {
            $resData['error'] = 'default';
            $resData['errorMessage'] = 'Аукцион завершен.';
            return response()->json($resData);
        }
        if (!$auction->end_date) {
            $auction->end_date = Carbon::now()->startOfDay()->addHours(21)->addMinutes(5)->addDay()->format('Y-m-d H:i:s');
            //$auction->end_date = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
            $auction->save();
        }
        $bid = new Bid();
        $auctionWithBids = $auction->load(['bids' => function ($query) {
            $query->orderBy('created_at', 'asc');
        }]);
        $lastBid = $auctionWithBids->bids->last() ?? null;
        $currentAuctionSum = $lastBid ? $lastBid->sum : $auction->price;
        if ($request->bid < ($currentAuctionSum + $auction->step) && $lastBid) {
            $resData['error'] = 'default';
            $resData['errorMessage'] = 'Ваша минимальная ставка - ' . ($currentAuctionSum + $auction->step) . 'р.';
            return response()->json($resData);
        }
        if ($lastBid && $lastBid->user_id == $userId) {
            $resData['error'] = 'default';
            $resData['errorMessage'] = 'Ваша ставка уже последняя. Дождитесь пока кто то сделает ставку выше вашей.';
            return response()->json($resData);
        }
        $bid->user_id = $userId;
        $bid->sum = $request->bid;
        $bid->auction_id = $id;
        $bid->save();
        $auction->touch();
        if ($lastBid) {
            \Mail::to($user->email)->send(new \App\Mail\BidLostTop($user, $auction, $bid));
        }
        $bid = $bid->load('user:id,login');
        $resData['success'] = true;
        $resData['bid'] = $bid;
        $resData['auction'] = Auction::with('images', 'user', 'bids.user:id,login')->where('active', 1)->find($auction->id);
        return response()->json($resData);
    }

    public function saveAuction(Request $request)
    {
        $v = self::validateCommon($request->all(), [
            'name' => 'required|string',
            'step' => 'required|numeric| min:1',
            'price'  => 'required|numeric:1',
            //'end_date' =>  'required|date_format:Y-m-d H:i|after:' . Carbon::now()->format('Y-m-d'),
            'description' =>  'nullable|string'
        ]);
        if ($v->fails()) {
            return self::errorValidateResponse($v->errors());
        }
        $auction = Auction::create([
            'name' => $request->name,
            'step' => $request->step,
            'price' => $request->price,
            //'end_date' => $request->end_date,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'active' => 0
        ]);

        $files = $request->file('files');
        if (!$request->hasFile('files')) {
            $auction->delete();
            return self::errorValidateResponse(['files' => ['Нужно загрузить хотя бы одно изображение']]);
        }
        $allowedExtensions = config('images.allowed_extensions');
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            if (!in_array($extension, $allowedExtensions)) {
                $auction->delete();
                return self::errorValidateResponse(['files' => ['Разрешенные форматы: jpg, jpeg, png и bmp']]);
            }
            $size = $file->getSize();
            if ($size > 1024 * 1024) {
                $auction->delete();
                return self::errorValidateResponse(['files' => ['Максимальный размер загружаемого изображения: 1Мб']]);
            }
            $originalName = $file->getClientOriginalName();
            $name = $file->hashName();
            $file->extension();
            $image = Image::make($file);
            $height = $image->height();
            $width = $image->width();

            $normalPx = config('images.normal_size');
            $prepath = substr(md5(microtime()), mt_rand(0, 30), 2) . '/' . substr(md5(microtime()), mt_rand(0, 30), 2);
            if ($height < $normalPx && $width < $normalPx) {
                $imageNormal = $image;
            } else {
                if ($height > $width) {
                    $k = $height / $normalPx;
                    $width = $width / $k;
                    $imageNormal = $image->resize($width, $normalPx, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $k = $width / $normalPx;
                    $height = $height / $k;
                    $imageNormal = $image->resize($normalPx, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }
            Storage::disk('public')->put($prepath . '/' . $name, (string) $imageNormal->encode());

            $maxpx = config('images.max_size');
            $height = $image->height();
            $width = $image->width();
            if ($height > $width) {
                $k = $width / $maxpx;
                $height = $height / $k;
                $imageMin = $image->resize($maxpx, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $k = $height / $maxpx;
                $width = $width / $k;
                $imageMin = $image->resize($width, $maxpx, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('public')->put($prepath . '/min_' . $name, (string) $imageMin->encode());
            //$file->move('storage/' . $prepath, $name);

            AuctionImage::create([
                'auction_id' => $auction->id,
                'path' => $prepath . '/' . $name,
                'path_min' => $prepath . '/min_' . $name,
                'original_name' => $originalName
            ]);
        }

        $resData['success'] = true;
        return response()->json($resData);
    }

    public function updateAuction(Request $request)
    {
        $auction = Auction::find($request->get('id'));
        $v = self::validateCommon($request->all(), [
            'name' => 'required|string',
            'step' => 'required|numeric| min:1',
            'price'  => 'required|numeric:1',
            //'end_date' =>  'required|date_format:Y-m-d H:i|after_or_equal:' . $auction->end_date,
            'description' =>  'nullable|string'
        ]);
        if ($v->fails()) {
            return self::errorValidateResponse($v->errors());
        }
        if (Auth::user()->role == USER::ADMIN) {
            $auction->active = $request->active;
            if ($auction->payed == 0 && $request->payed == 1) {
                $oUser = $auction->load('user')->user;
                $winBid = $auction->bids()->latest()->first();
                if (!$winBid) {
                    return self::errorValidateResponse(['payed' => ['Отсутствует победитель']]);
                }
                $oUserWinner = $winBid->load('user')->user;
                $dialog = Dialog::where([
                    ['creator_id', $oUser->id],
                    ['with_id', $oUserWinner->id]
                ])->orWhere([
                    ['creator_id',$oUserWinner->id],
                    ['with_id', $oUser->id]
                ])->first();
                if (!$dialog) {
                    $dialog = new Dialog();
                    $dialog->creator_id = $oUser->id;
                    $dialog->with_id = $oUserWinner->id;
                    $dialog->save();
                }
                $message = new Message();
                $message->user_id = $oUserWinner->id;
                $message->user_to_id = $oUser->id;
                $message->dialog_id = $dialog->id;
                $message->type = Message::SYSTEM;
                $message->message = 'Аукцион оплачен';
                $message->save();
            }
            $auction->payed = $request->payed;
            $auction->save();
        }

        $saveUploadData = [];
        $files = $request->file('files');
        if ($request->hasFile('files')) {
            $allowedExtensions = config('images.allowed_extensions');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                if (!in_array($extension, $allowedExtensions)) {
                    return self::errorValidateResponse(['files' => ['Разрешенные форматы: jpg, jpeg, png и bmp']]);
                }
                $size = $file->getSize();
                if ($size > 1024 * 1024) {
                    return self::errorValidateResponse(['files' => ['Максимальный размер загружаемого изображения: 1Мб']]);
                }
                $originalName = $file->getClientOriginalName();
                $name = $file->hashName();

                $image = Image::make($file);
                $height = $image->height();
                $width = $image->width();

                $normalPx = config('images.normal_size');
                $prepath = substr(md5(microtime()), mt_rand(0, 30), 2) . '/' . substr(md5(microtime()), mt_rand(0, 30), 2);
                if ($height < $normalPx && $width < $normalPx) {
                    $imageNormal = $image;
                } else {
                    if ($height > $width) {
                        $k = $height / $normalPx;
                        $width = $width / $k;
                        $imageNormal = $image->resize($width, $normalPx, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $k = $width / $normalPx;
                        $height = $height / $k;
                        $imageNormal = $image->resize($normalPx, $height, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                }
                Storage::disk('public')->put($prepath . '/' . $name, (string) $imageNormal->encode());
                $maxpx = config('images.max_size');
                if ($height > $width) {
                    $k = $width / $maxpx;
                    $height = $height / $k;
                    $imageMin = $image->resize($maxpx, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $k = $height / $maxpx;
                    $width = $width / $k;
                    $imageMin = $image->resize($width, $maxpx, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                Storage::disk('public')->put($prepath . '/min_' . $name, (string) $imageMin->encode());

                $saveUploadData[] = [
                    'auction_id' => $auction->id,
                    'path' => $prepath . '/' . $name,
                    'path_min' => $prepath . '/min_' . $name,
                    'original_name' => $originalName
                ];
            }
        }
        $auction->name = $request->name;
        $auction->step = $request->step;
        $auction->price = $request->price;
        //$auction->end_date = Carbon::createFromFormat('Y-m-d H:i', $request->end_date)->format('Y-m-d H:i:s');
        $auction->description = $request->description;
        $auction->save();
        $filesToChange = $request->get('filesToChange');
        AuctionImage::where('auction_id', $auction->id)->delete();
        $start = 0;
        if (!empty($filesToChange[0]) &&  json_decode($filesToChange[0], true)['position'] > $start) {
            foreach ($saveUploadData as $k => $data) {
                if ($k >= json_decode($filesToChange[0], true)['position']) {
                    continue;
                }
                AuctionImage::create($data);
                unset($saveUploadData[$k]);
                $start++;
            }
        }
        $startUploaded = 0;

        if (!empty($filesToChange[0])) {
            foreach ($filesToChange as $key1 => $fileExist) {
                $fileExist = json_decode($fileExist, true);
                $pathMin = dirname($fileExist['img']) . '/min_' . basename($fileExist['img']);

                if ($fileExist['deleted']) {
                    try {
                        unlink(public_path(substr($fileExist['img'], 1)));
                    } catch (\Exception $e) {
                        //
                    };
                    $start++;
                } else if ($fileExist['position'] <= $start) {
                    AuctionImage::create([
                        'auction_id' => $auction->id,
                        'path' => str_replace('/storage/', '', $fileExist['img']),
                        'path_min' => str_replace('/storage/', '', $pathMin),
                        'original_name' => $fileExist['name']
                    ]);
                    $start++;
                } else {
                    $length = $fileExist['position'] - $start;
                    foreach ($saveUploadData as $key2 => $data) {
                        if ($key2 < $startUploaded) {
                            continue;
                        }
                        if ($key2 > $startUploaded + $length) {
                            continue;
                        }
                        AuctionImage::create($data);
                        unset($saveUploadData[$key2]);
                        $start++;

                        AuctionImage::create([
                            'auction_id' => $auction->id,
                            'path' => str_replace('/storage/', '', $fileExist['img']),
                            'path_min' => str_replace('/storage/', '', $pathMin),
                            'original_name' => $fileExist['name']
                        ]);
                    }
                    $startUploaded = $startUploaded + $length;
                }
            }
        }

        foreach ($saveUploadData as $data) {
            AuctionImage::create($data);
        }

        $resData['success'] = true;
        return response()->json($resData);
    }
}
