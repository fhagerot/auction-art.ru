<?php

namespace App\Http\Controllers;

use App\AuctionImage;
use App\Dialog;
use App\Message;
use App\Traits\ValidationTrait;
use App\User;
use App\MessageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Media\Entities\File;
use App\Events\MessageEvent;

class ChatController extends Controller
{
    use ValidationTrait;

    public function loadDialogs(Request $request)
    {
        $dialogs = Dialog::has('messages')->with(['messages' => function ($q) {
            return $q->latest();
        }])
        ->where(function ($query) {
            $query->where('creator_id', Auth::user()->id)
                ->orWhere('with_id', Auth::user()->id);
        })->get();
        $resData['dialogs'] = $dialogs;
        $resData['success'] = true;
        return response()->json($resData);
    }

    public function message(Request $request)
    {
        if (!User::where('id', $request->user_to_id)->exists()) {
            return response()->json(['error' => 'wrong_dialog']);
        }
        $files = $request->file('files');
        if (!$files && !$request->message) {
            return self::errorValidateResponse(['message' => ['Пустое сообщение']]);
        }
        $dialog = Dialog::where([
            ['creator_id', Auth::user()->id],
            ['with_id', $request->user_to_id]
        ])->orWhere([
            ['creator_id', $request->user_to_id],
            ['with_id', Auth::user()->id]
        ])->first();
        if (!$dialog) {
            $dialog = new Dialog();
            $dialog->creator_id = Auth::user()->id;
            $dialog->with_id = $request->user_to_id;
            $dialog->save();
        }
        $message = new Message();
        $message->user_id = Auth::user()->id;
        $message->user_to_id = $request->user_to_id;
        $message->dialog_id = $dialog->id;
        $message->message = $request->message;
        $message->save();

        $allowedExtensions = config('images.allowed_extensions');
        if ($files) {
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                if (!in_array($extension, $allowedExtensions)) {
                    $message->delete();
                    return self::errorValidateResponse(['files' => ['Разрешенные форматы: jpg, jpeg, png и bmp']]);
                }
                $size = $file->getSize();
                if ($size > 1024 * 1024) {
                    $message->delete();
                    return self::errorValidateResponse(['files' => ['Максимальный размер загружаемого изображения: 1Мб']]);
                }
                $originalName = $file->getClientOriginalName();
                $name = $file->hashName();
                $file->extension();
                $image = Image::make($file);
                $height = $image->height();
                $width = $image->width();

                $normalPx = config('images.normal_size');
                $prepath = substr(md5(microtime()), mt_rand(0, 30), 2) . '/' . substr(md5(microtime()), mt_rand(0, 30), 2);
                if ($height < $normalPx && $width < $normalPx) {
                    $imageNormal = $image;
                } else {
                    if ($height > $width) {
                        $k = $height / $normalPx;
                        $width = $width / $k;
                        $imageNormal = $image->resize($width, $normalPx, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $k = $width / $normalPx;
                        $height = $height / $k;
                        $imageNormal = $image->resize($normalPx, $height, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                }
                Storage::disk('public')->put($prepath . '/' . $name, (string)$imageNormal->encode());

                $maxpx = config('images.max_size');
                $height = $image->height();
                $width = $image->width();
                if ($height > $width) {
                    $k = $width / $maxpx;
                    $height = $height / $k;
                    $imageMin = $image->resize($maxpx, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $k = $height / $maxpx;
                    $width = $width / $k;
                    $imageMin = $image->resize($width, $maxpx, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                Storage::disk('public')->put($prepath . '/min_' . $name, (string)$imageMin->encode());
                Log::info('savee');
                MessageImage::create([
                    'message_id' => $message->id,
                    'path' => $prepath . '/' . $name,
                    'path_min' => $prepath . '/min_' . $name,
                    'original_name' => $originalName
                ]);
            }
        }
        $message = $message->load('user');
        $message = $message->load('images');
        event(new MessageEvent($message));
        $resData['message'] = $message;
        $resData['success'] = true;
        return response()->json($resData);
    }

    public function loadMessages(Request $request)
    {
        if (!User::where('id', $request->user_to_id)->exists()) {
            return response()->json(['error' => 'not_found']);
        }
        $user = Auth::user();
        $dialog = Dialog::where([
            ['creator_id',  $user->id],
            ['with_id', $request->user_to_id]
        ])->orWhere([
            ['creator_id', $request->user_to_id],
            ['with_id',  $user->id]
        ])->first();
        if (!$dialog) {
            $dialog = new Dialog();
            $dialog->creator_id =  $user->id;
            $dialog->with_id = $request->user_to_id;
            $dialog->save();
        }
        Message::where([
            ['dialog_id', $dialog->id],
            ['readed', false],
            ['user_to_id',  $user->id]
        ])->update([
            'readed' => true
        ]);
        $messages = Message::where('dialog_id', $dialog->id)->with('user')->with('images')->get();
        $resData['success'] = true;
        $resData['messages'] = $messages;
        $resData['dialog'] = $dialog;
        return response()->json($resData);
    }
}
