<?php

namespace App\Http\Controllers;

use App\Auction;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
    {
        $auctions = Auction::where('active', 1)->get();
        return response()->view('sitemap.index', [
            'auctions' => $auctions,
        ])->header('Content-Type', 'text/xml');
    }
}
