<?php

namespace App\Http\Controllers;

use App\Traits\ValidationTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use ValidationTrait;

    public function register(Request $request)
    {
        $v = self::validateCommon($request->all(), [
            'email' => 'required|email|unique:users',
            'login' => 'required|unique:users|min:4|max:22|regex:/^[0-9A-Za-z_-]+$/',
            'password'  => 'required|min:3|confirmed',
            'role' =>  ['required', 'integer', Rule::in([1, 3])]
        ]);
        if ($v->fails()) {
            return self::errorValidateResponse($v->errors());
        }
        $user = new User();
        $user->email = $request->email;
        $user->login = $request->login;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success'], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
           /* if (Auth::user()->active != 1) {
                $this->guard()->logout();
                return response()->json(['error' => 'login_error'], 401);
            }*/
            return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
        }
        $this->guard()->logout();
        return response()->json(['error' => 'login_error'], 200);
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function roles(Request $request)
    {
        $clientRoles = User::CLIENT_ROLES;
        return response()->json([
            'status' => 'success',
            'clientRoles' => $clientRoles
        ]);
    }

    public function refresh()
    {
        //JWTAuth::parseToken()->refresh();
        //Log::info(Auth::user()->email);

        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        auth()->logout();
        JWTAuth::invalidate(JWTAuth::parseToken());
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    public function resetPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return self::errorValidateResponse(['email' => ['Пользователь не найден']]);
        }
        $resethash = dechex(time()) . md5(uniqid($user->email));
        $user->access_hash = Hash::make($resethash);
        $user->save();
        \Mail::to($user->email)->send(new \App\Mail\ResetPassword($user, $resethash));
        return response()->json(['success' => true], 200);
    }

    public function changePassword(Request $request)
    {
        $v = self::validateCommon($request->all(), [
            'password'  => 'required|min:3|confirmed',
            'email'  => 'email|required',
        ]);
        if ($v->fails()) {
            return self::errorValidateResponse($v->errors());
        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return self::errorValidateResponse(['email' => ['Пользователь не найден']]);
        }
        $access_hash = $request->access_hash;
        if (Hash::check($access_hash, $user->access_hash) && $user->access_hash) {
            $user->access_hash = null;
            $user->password = bcrypt($request->password);
            $user->save();
        } else {
            return self::errorValidateResponse(['access_hash' => ['Не верная шифрованная подпись']]);
        }
        return response()->json(['success' => true], 200);
    }

    private function guard()
    {
        return Auth::guard();
    }
}
