<?php

namespace App\Http\AdminControllers;

use App\Auction;
use App\Http\Controllers\Controller;
use App\Traits\ValidationTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    use ValidationTrait;

    public function getUser(Request $request)
    {
        $user = User::withoutGlobalScope('active')->find($request->id);

        $resData['success'] = true;
        $resData['user'] = $user;
        return response()->json($resData);
    }

    public function updateUser(Request $request)
    {
        $user = User::withoutGlobalScope('active')->find($request->id);

        $v = self::validateCommon($request->all(), [
            'active' =>  ['required', 'integer']
        ]);
        $user->active = $request->active;
        $user->save();
        if ($v->fails()) {
            return self::errorValidateResponse($v->errors());
        }
        $resData['success'] = true;
        return response()->json($resData);
    }
}
