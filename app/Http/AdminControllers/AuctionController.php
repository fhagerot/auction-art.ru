<?php

namespace App\Http\AdminControllers;

use App\Auction;
use App\Http\Controllers\Controller;

class AuctionController extends Controller
{
    public function index()
    {
        $auctions = Auction::with('images', 'bids', 'user')->orderBy('created_at', 'desc')->paginate(15);
        return response()->json($auctions);
    }
}
