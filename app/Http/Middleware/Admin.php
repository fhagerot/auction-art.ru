<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->isAdmin()) {
            return response()->json(['error' => 'You are not admin']);
        }
        return $next($request);
    }
}
