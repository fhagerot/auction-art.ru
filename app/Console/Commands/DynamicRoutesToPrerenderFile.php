<?php

namespace App\Console\Commands;

use App\Auction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DynamicRoutesToPrerenderFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prerender_dynamic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Put dynamic routes to txt file wich will be readed from webpack.mix.js and prerender';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auctions = Auction::where([
            ['updated_at', '>',  Carbon::now()->subHours(2)],
            ['prerender_queued', '=', 0]
        ])->get();
        $dynamicRoutes = [];
        foreach ($auctions as $auction) {
            $dynamicRoutes[] = '/auctions/' . $auction->id;
            $auction->prerender_queued = 1;
            $auction->save();
        }
        $open = fopen(base_path('dynamic_routes.txt'), 'r+');
        $exist = [];
        $delimiter = "\n";
        if (DIRECTORY_SEPARATOR === '\\') {
            $delimiter = "\r\n";
        }
        if ($open && filesize(base_path('dynamic_routes.txt')) > 0) {
            $exist = explode($delimiter, fread($open, filesize(base_path('dynamic_routes.txt'))));
        }
        $dynamicRoutes = array_diff($dynamicRoutes, $exist);
        $open = fopen(base_path('dynamic_routes.txt'), 'a+');
        if (filesize(base_path('dynamic_routes.txt')) > 0) {
            fwrite($open, PHP_EOL);
        }
        foreach ($dynamicRoutes as $key => $value) {
            if (next($dynamicRoutes) == true) {
                $value .= PHP_EOL;
            }
            fwrite($open, $value);
        }
    }
}
