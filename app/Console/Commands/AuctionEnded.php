<?php

namespace App\Console\Commands;

use App\Auction;
use App\Dialog;
use App\Events\MessageEvent;
use App\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AuctionEnded extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auctions_ended';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auctions ended';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auctions = Auction::where([
            ['message_sended', 0],
            ['active', 1],
            ['end_date', '<', Carbon::now()->format('Y-m-d H:i:s')]
        ])->get();
        foreach ($auctions as $auction) {
            $winBid = $auction->bids()->latest()->first();
            if ($winBid) {
                $oUserWinner = $winBid->load('user')->user;
                \Mail::to($oUserWinner->email)->send(new \App\Mail\Winner($oUserWinner, $auction));
                $oUser = $auction->load('user')->user;
                \Mail::to($oUser->email)->send(new \App\Mail\AuctionGetWinner($oUser, $auction, $oUserWinner));

                $dialog = Dialog::where([
                    ['creator_id', User::ADMIN_ID],
                    ['with_id', $oUserWinner->id]
                ])->orWhere([
                    ['creator_id',$oUserWinner->id],
                    ['with_id', User::ADMIN_ID]
                ])->first();
                if (!$dialog) {
                    $dialog = new Dialog();
                    $dialog->creator_id = User::ADMIN_ID;
                    $dialog->with_id = $oUserWinner->id;
                    $dialog->save();
                }
                $message = new Message();
                $message->user_id = User::ADMIN_ID;
                $message->user_to_id = $oUserWinner->id;
                $message->dialog_id = $dialog->id;
                $message->type = Message::SYSTEM;
                $message->message = 'Здравствуйте! Вы победили в аукционе <a href="/auctions/' . $auction->id . '">' . $auction->name . '</a> Оплатите пожалуста по номеру карты сбербанка 5469990010554272 (Глеб Александрович Ю.) ' . $winBid->price . ' р. после чего  <a href="/dashboard/dialogs/' . $oUser->id . '">создатель аукциона</a> отправит вам вашу картину. Деньги поступят на счет создателя аукциона только после получения вами картины, в противном случае деньги вам будут возвращены';
                $message->save();

                $dialog = Dialog::where([
                    ['creator_id', $oUser->id],
                    ['with_id', $oUserWinner->id]
                ])->orWhere([
                    ['creator_id',$oUserWinner->id],
                    ['with_id', $oUser->id]
                ])->first();
                if (!$dialog) {
                    $dialog = new Dialog();
                    $dialog->creator_id = $oUser->id;
                    $dialog->with_id = $oUserWinner->id;
                    $dialog->save();
                }
                $message = new Message();
                $message->user_id = $oUser->id;
                $message->user_to_id = $oUserWinner->id;
                $message->dialog_id = $dialog->id;
                $message->type = Message::SYSTEM;
                $message->message = 'Здравствуйте. Я автор аукциона (<a href="/auctions/' . $auction->id . '">' . $auction->name . '</a>) в котором вы победили';
                $message->save();

                $auction->message_sended = 1;
                $auction->save();
                $this->line('message sended');
               //event(new MessageEvent($message));
            }
        }
    }
}
