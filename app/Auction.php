<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use BeyondCode\Comments\Traits\HasComments;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Auction extends Model
{
    use HasComments;

    protected $appends = ['counter', 'formatted_end_date', 'formatted_created', 'ended'];
    protected $fillable = ['name', 'description', 'user_id', 'price', 'step', 'active', 'end_date'];

    const TYPE_OF_PAYMENTS = [
        'bank_transfer' => 'Банковский перевод',
        'cash' => 'Наличными'
    ];

    public static function boot()
    {
        parent::boot();

        self::updating(function ($auction) {
            if (!($auction->getOriginal('prerender_queued') == 0 && $auction->prerender_queued == 1)) {
                $auction->prerender_queued = 0;
            }
        });
    }

    protected $casts = [
        'end_date' => 'datetime:d.m.y H:i',
    ];

    public function images()
    {
        return $this->hasMany(AuctionImage::class);
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCounterAttribute()
    {
        $now = Carbon::now();
        return $now->diffInSeconds($this->end_date, false);
    }

    public function getFormattedEndDateAttribute()
    {
        if ($this->end_date !== null) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date)->format('d.m.y H:i:s');
        }
        return false;
    }

    public function getEndedAttribute()
    {
        if ($this->end_date) {
            return $this->end_date < Carbon::now()->format('Y-m-d H:i:s');
        }
        return false;
    }

    public function getFormattedCreatedAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d.m.y H:i');
    }
}
