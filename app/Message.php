<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    const SYSTEM = 'system';
    const DEFAULT = 'default';

    protected $appends = ['formatted_date', 'owner'];
    protected $fillable = ['user_id', 'user_to_id', 'dialog_id', 'message', 'readed'];

    public function getFormattedDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d.m.y H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(MessageImage::class);
    }
    public function getOwnerAttribute()
    {
        if (Auth::user()) {
            return $this->user_id == Auth::user()->id ? true : false;
        }
        return false;
    }
}
