<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait ValidationTrait
{
    public static function validateCommon($request, $data)
    {
        return Validator::make(
            $request,
            $data,
            config('validationlang.messages'),
            config('validationlang.customAttributes')
        );
    }
    public static function errorValidateResponse($fails)
    {
        return response()->json([
            'error' => true,
            'errors' => $fails
        ], 200);
    }
}
