<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ActiveTrait
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('active', '=', 1);
        });
    }
}
