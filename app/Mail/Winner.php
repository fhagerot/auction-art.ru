<?php

/**
 * @copyright
 * @author
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class for device.
 */
class Winner extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var mixed
     */
    private $oUser;
    private $auction;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var null|string
     */
    private $type;

    /**
     * Device constructor.
     * @param string $type
     * @param mixed $oUser
     * @param array $data
     */
    public function __construct($oUser, $auction)
    {
        $this->oUser = $oUser;
        $this->auction = $auction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = config('app.name') . ' - поздравляем с победой в аукционе!';
        $result = $this->view('emails.win')->with([
            'title'    => $title,
            'oUser'    => $this->oUser,
            'auction' => $this->auction,
        ])->subject($title)->from(config('mail.username'), 'Арт Аукцион');
        return $result;
    }
}
