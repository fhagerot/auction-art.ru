<?php

/**
 * @copyright
 * @author
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class for device.
 */
class BidLostTop extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var mixed
     */
    private $oUser;
    private $auction;
    private $lastBid;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var null|string
     */
    private $type;

    /**
     * Device constructor.
     * @param string $type
     * @param mixed $oUser
     * @param array $data
     */
    public function __construct($oUser, $auction, $lastBid)
    {
        $this->oUser = $oUser;
        $this->auction = $auction;
        $this->lastBid = $lastBid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = config('app.name') . ' - ваша ставка перебита!';
        $result = $this->view('emails.bid_lost_top')->with([
            'title'    => $title,
            'oUser'    => $this->oUser,
            'auction' => $this->auction,
            'lastBid' => $this->lastBid
        ])->subject($title)->from(config('mail.username'), 'Арт Аукцион');
        return $result;
    }
}
