<?php

/**
 * @copyright
 * @author
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class for device.
 */
class AuctionGetWinner extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var mixed
     */
    private $oUser;
    private $auction;
    private $winner;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var null|string
     */
    private $type;

    /**
     * Device constructor.
     * @param string $type
     * @param mixed $oUser
     * @param array $data
     */
    public function __construct($oUser, $auction, $winner)
    {
        $this->oUser = $oUser;
        $this->auction = $auction;
        $this->winner = $winner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = config('app.name') . ' - у аукциона есть победитель!';
        $result = $this->view('emails.get_winner')->with([
            'title'    => $title,
            'oUser'    => $this->oUser,
            'auction' => $this->auction,
            'winner' => $this->winner,
        ])->subject($title)->from(config('mail.username'), 'Арт Аукцион');
        return $result;
    }
}
