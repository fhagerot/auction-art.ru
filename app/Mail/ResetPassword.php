<?php

/**
 * @copyright
 * @author
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class for device.
 */
class ResetPassword extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var mixed
     */
    private $oUser;
    private $access_hash;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var null|string
     */
    private $type;

    /**
     * Device constructor.
     * @param string $type
     * @param mixed $oUser
     * @param array $data
     */
    public function __construct($oUser, $access_hash)
    {
        $this->oUser = $oUser;
        $this->access_hash = $access_hash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = config('app.name') . ' - cмена пароля.';
        $result = $this->view('emails.reset_password')->with([
            'title'    => $title,
            'oUser'    => $this->oUser,
            'access_hash' => $this->access_hash,
        ])->subject($title)->from(config('mail.username'), 'Арт Аукцион');
        return $result;
    }
}
