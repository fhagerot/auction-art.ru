<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageImage extends Model
{
    protected $fillable = ['message_id', 'path', 'original_name', 'path_min'];

    public function getPathAttribute($path)
    {
        return '/storage/' . $path;
    }

    public function getPathMinAttribute($path)
    {
        return '/storage/' . $path;
    }
}
