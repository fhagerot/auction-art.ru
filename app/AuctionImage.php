<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuctionImage extends Model
{
    protected $fillable = ['auction_id', 'path', 'original_name', 'path_min'];

    public function getPathAttribute($path)
    {
        return '/storage/' . $path;
    }

    public function getPathMinAttribute($path)
    {
        return '/storage/' . $path;
    }
}
