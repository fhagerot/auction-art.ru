<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Dialog extends Model
{
    protected $appends = ["name", "to", "count_unreaded"];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function getToAttribute()
    {
        if ($this->creator_id == Auth::user()->id) {
            return $this->with_id;
        }
        return $this->creator_id;
    }

    public function getCountUnreadedAttribute()
    {
        return $this->messages()->where([
            ['readed', false],
            ['user_to_id',  Auth::user()->id],
        ])->count();
    }
    public function getNameAttribute()
    {
        if (($this->creator_id == User::ADMIN_ID || $this->with_id == User::ADMIN_ID) && Auth::user()->id !== User::ADMIN_ID) {
            return 'Администратор';
        }
        if ($this->creator_id == Auth::user()->id) {
            return User::where('id', $this->with_id)->value('login');
        }
        return User::where('id', $this->creator_id)->value('login');
    }
}
