var args = process.argv.slice(2);
if (args.includes('production')) {
    var fs = require('fs');

    var options = {
        key: fs.readFileSync('/etc/ssl/auction-art.ru/private.key'),
        cert: fs.readFileSync('/etc/ssl/auction-art.ru/res.crt')
    };
    var https = require('https').Server(options);
    var io = require('socket.io')(https);
    var Redis = require('ioredis');

    var redis = new Redis();
    redis.psubscribe('new-message.*');
    redis.on('pmessage', function(pattern, channel, message) {
        console.log('message received  ' + message);
        console.log('Channel ' + channel);
        message = JSON.parse(message);
        console.log(channel + ':' + message.event);
        io.emit(channel + ':' + message.event, message.data);
    });

    https.listen(32768, function() {
        console.log('Listening on Port: 32768');
    });

} else {
    var http = require('http').Server();
    var io = require('socket.io')(http);
    var Redis = require('ioredis');

    var redis = new Redis();
    redis.psubscribe('new-message.*');
    redis.on('pmessage', function (pattern, channel, message) {
        console.log('message received  ' + message);
        console.log('Channel ' + channel);
        message = JSON.parse(message);
        console.log(channel + ':' + message.event);
        io.emit(channel + ':' + message.event, message.data);
    });

    http.listen(3000, function () {
        console.log('Listening on Port: 3000');
    });
}
